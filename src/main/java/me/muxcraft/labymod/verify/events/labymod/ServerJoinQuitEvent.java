package me.muxcraft.labymod.verify.events.labymod;

import io.netty.buffer.ByteBufUtil;
import me.muxcraft.labymod.verify.MuxVerify;
import net.labymod.utils.ServerData;
import net.minecraft.network.PacketBuffer;

public class ServerJoinQuitEvent {


    private final MuxVerify muxVerify;

    public ServerJoinQuitEvent(MuxVerify muxVerify){
        this.muxVerify = muxVerify;
        muxVerify.getApi().getEventManager().registerOnJoin(this::onJoin);
        muxVerify.getApi().getEventManager().registerOnQuit(this::onQuit);
    }


    private void onJoin(ServerData serverData){

        String ip = serverData.getIp();
        boolean isMux = false;
        for(String domain : muxVerify.getDomains()){
            if(domain.toLowerCase().contains(ip.toLowerCase())){
                isMux = true;
                break;
            }
        }
        if(isMux){
            muxVerify.setEnabled(!muxVerify.isEnabled());
            PacketBuffer packetBuffer = new PacketBuffer(ByteBufUtil.threadLocalDirectBuffer());
            packetBuffer.writeString("sha1=" + muxVerify.getA());
            muxVerify.getApi().sendPluginMessage("Mux-Verify",packetBuffer);
        }
    }

    private void onQuit(ServerData serverData){
        if(muxVerify.isEnabled())
            muxVerify.setEnabled(!muxVerify.isEnabled());
    }
}
