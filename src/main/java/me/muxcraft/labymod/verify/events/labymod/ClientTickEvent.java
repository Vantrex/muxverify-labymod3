package me.muxcraft.labymod.verify.events.labymod;

import me.muxcraft.labymod.verify.MuxVerify;
import me.muxcraft.labymod.verify.anticheat.impl.Misplace;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;

public class ClientTickEvent{

    private int tick = 0;


    private int i = 20;
    private int j = 1;

    private final MuxVerify muxVerify;


    public ClientTickEvent(MuxVerify muxVerify){
        this.muxVerify = muxVerify;
        this.muxVerify.getApi().registerForgeListener(this);
    }

    @SubscribeEvent
    public void onTick(TickEvent.ClientTickEvent event){
        if(!muxVerify.isEnabled()) return;
        tick++;
        if(tick == 20){
            secondLoop();
            tick = 0;
        }
    }
    private void secondLoop(){
        i++;
        if(i == 30){
            i = 0;
            thirtySecondLoop();

        }
    }
    private void thirtySecondLoop(){
        j++;
        if(j == 2){
            j = 0;
            oneMinuteLoop();
            muxVerify.getDetectionManager().getProcessDetection().checkProcesses();
        }
    }
    private void oneMinuteLoop(){

    }


}
