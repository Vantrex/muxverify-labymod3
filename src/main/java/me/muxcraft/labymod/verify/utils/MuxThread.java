package me.muxcraft.labymod.verify.utils;

import me.muxcraft.labymod.verify.MuxVerify;
import me.muxcraft.labymod.verify.anticheat.impl.Misplace;

public class MuxThread extends Thread{

    private final MuxVerify muxVerify;
    private final Misplace misplace;
    public MuxThread(MuxVerify muxVerify){
        this.muxVerify = muxVerify;
        misplace = new Misplace(muxVerify);
    }

    @Override
    public void run() {
        while (true){
            if(muxVerify.isEnabled()){
                misplace.onOverride();
                muxVerify.getDetectionManager().getProcessDetection().checkProcesses();
            }
            try {
                Thread.sleep(30000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
