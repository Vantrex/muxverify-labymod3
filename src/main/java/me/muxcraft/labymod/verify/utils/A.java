package me.muxcraft.labymod.verify.utils;
import me.muxcraft.labymod.verify.MuxVerify;
import net.labymod.addon.AddonLoader;
import sun.misc.BASE64Encoder;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URISyntaxException;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class A {
    public String A() throws NoSuchAlgorithmException, IOException, URISyntaxException {
        File file = AddonLoader.getJarFileByClass(MuxVerify.class);
        final String fileName = file.getAbsolutePath();
        byte[] buffer= new byte[8192];
        int count;
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        BufferedInputStream bis = new BufferedInputStream(new FileInputStream(fileName));
        while ((count = bis.read(buffer)) > 0) {
            digest.update(buffer, 0, count);
        }
        bis.close();

        byte[] hash = digest.digest();

        return new BASE64Encoder().encode(hash);
    }

}
