package me.muxcraft.labymod.verify.anticheat.impl;
import io.netty.buffer.ByteBufUtil;
import me.muxcraft.labymod.verify.anticheat.Detection;
import net.minecraft.network.PacketBuffer;
import java.io.IOException;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.*;

public class ProcessDetection extends Detection{

    private final List<String> processList;

    public ProcessDetection() {
        super("Detection");
        this.processList = new ArrayList<>();
        try {
            URL url = new URL("http://muxcraft.eu/verify/processlist.txt");
            Scanner s = new Scanner(url.openStream());
            while (s.hasNext())
                processList.add(s.nextLine());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


        public void checkProcesses(){
        if(!muxVerify.isWindows()) return;
        if(!muxVerify.isEnabled()) return;

        new Thread(() -> {
            Process process = null;
            try {
                process = new ProcessBuilder("tasklist.exe", "/fo", "csv", "/nh").start();
            } catch (IOException e) {
                e.printStackTrace();
            }
            final Process finalProcess = process;
            Scanner sc = new Scanner(finalProcess.getInputStream());
            if (sc.hasNextLine()) sc.nextLine();
            while (sc.hasNextLine()) {
                String line = sc.nextLine();
                String[] parts = line.split(",");
                String unq = parts[0].substring(1).replaceFirst(".$", "");
                for(String s : this.processList){
                    if(s.equalsIgnoreCase(unq)){
                        onFail(s);
                    }
                }
                try {
                    process.waitFor();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();

    }
    @Override
    public void onFail(String... reason) {
        muxVerify.getApi().displayMessageInChat(reason[0]);
        PacketBuffer packetBuffer = new PacketBuffer(ByteBufUtil.threadLocalDirectBuffer());
        packetBuffer.writeString("anticheat_failed=process_detection>" + reason[0]);
        muxVerify.getApi().sendPluginMessage("Mux-Verify",packetBuffer);
    }
}
