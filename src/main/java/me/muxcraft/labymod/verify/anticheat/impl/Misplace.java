package me.muxcraft.labymod.verify.anticheat.impl;


import io.netty.buffer.ByteBufUtil;
import me.muxcraft.labymod.verify.MuxVerify;
import me.muxcraft.labymod.verify.anticheat.handler.MuxNetHandler;
import net.minecraft.client.network.NetHandlerPlayClient;
import net.minecraft.network.INetHandler;
import net.minecraft.network.PacketBuffer;

public class Misplace{

    private final MuxVerify muxVerify;

    public Misplace(MuxVerify muxVerify){
        this.muxVerify = muxVerify;
    }
    private int i = 0;
    public void onOverride(){
        INetHandler parent = muxVerify.getMC().thePlayer.sendQueue.getNetworkManager().getNetHandler();
        if (!(parent instanceof MuxNetHandler)) {
            i++;
            muxVerify.getMC().thePlayer.sendQueue.getNetworkManager().setNetHandler(new MuxNetHandler((NetHandlerPlayClient)parent));

            if(i >= 10){
                PacketBuffer packetBuffer = new PacketBuffer(ByteBufUtil.threadLocalDirectBuffer());
                muxVerify.getApi().displayMessageInChat("misplace");
                packetBuffer.writeString("anticheat_failed=reach>" + "misplace");
                muxVerify.getApi().sendPluginMessage("Mux-Verify",packetBuffer);
            }
        }
    }
}