package me.muxcraft.labymod.verify.anticheat;

import me.muxcraft.labymod.verify.MuxVerify;
import me.muxcraft.labymod.verify.anticheat.impl.Misplace;
import net.labymod.api.events.PluginMessageEvent;
import net.minecraft.client.Minecraft;
import net.minecraft.network.PacketBuffer;

import java.util.Timer;
import java.util.TimerTask;

public abstract class Detection {

    private final String name;

    protected final MuxVerify muxVerify;

    protected final Minecraft mc;

    public Detection(String name){
        this.name = name;
        this.muxVerify = MuxVerify.getInstance();
        this.mc = muxVerify.getMC();



    }

    public abstract void onFail(String... reason);

    public String getName() {
        return name;
    }
}
