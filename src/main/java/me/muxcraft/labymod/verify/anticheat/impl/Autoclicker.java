package me.muxcraft.labymod.verify.anticheat.impl;

import io.netty.buffer.ByteBufUtil;
import me.muxcraft.labymod.verify.MuxVerify;
import me.muxcraft.labymod.verify.anticheat.Detection;
import net.minecraft.client.Minecraft;
import net.minecraft.network.PacketBuffer;
import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;
import org.jnativehook.keyboard.NativeKeyEvent;
import org.jnativehook.keyboard.NativeKeyListener;
import org.jnativehook.mouse.NativeMouseEvent;
import org.jnativehook.mouse.NativeMouseListener;
import org.lwjgl.input.Mouse;


public class Autoclicker extends Detection implements NativeMouseListener, NativeKeyListener {

    private long lastLag = 0;

    public Autoclicker() {
        super("Autoclicker");
        if(!MuxVerify.getInstance().isWindows()) return;
        try {
            GlobalScreen.registerNativeHook();
        } catch (NativeHookException e) {
            e.printStackTrace();
        }
        GlobalScreen.addNativeMouseListener(this);
        GlobalScreen.addNativeKeyListener(this);
        this.muxVerify.getApi().registerForgeListener(this);
    }



    @Override
    public void onFail(String... reason) {
        muxVerify.getApi().displayMessageInChat(reason[0]);
        PacketBuffer packetBuffer = new PacketBuffer(ByteBufUtil.threadLocalDirectBuffer());
        packetBuffer.writeString("anticheat_failed="+reason[0]);
        muxVerify.getApi().sendPluginMessage("Mux-Verify",packetBuffer);
    }

    @Override
    public void nativeMouseClicked(NativeMouseEvent event) {


        if(Minecraft.getDebugFPS() < 10) return;

        if(!muxVerify.isEnabled()) return;

        if(!muxVerify.getApi().hasGameFocus()) return;

        if(lastLag > System.currentTimeMillis()) return;

        if(event.getButton() == 1){
            if(!Mouse.isButtonDown(0)) {
                clicker++;
                if(l <= System.currentTimeMillis()){
                    l = System.currentTimeMillis() + 60000;
                    clicker = 0;
                }
                if(clicker == 10) {
                    onFail("Clicker");
                    clicker = 0;
                }
            }else clicker = 0;
        }else if(event.getButton() == 2){
            if(!Mouse.isButtonDown(1)){
                clicker++;
                if(l <= System.currentTimeMillis()){
                    l = System.currentTimeMillis() + 60000;
                    clicker = 0;
                }
                if(clicker == 10) {
                    onFail();
                    clicker = 0;
                }
            }else clicker = 0;
        }
    }

    private int clicker = 0;

    private long l = 0;

    @Override
    public void nativeMousePressed(NativeMouseEvent event) {

    }




    @Override
    public void nativeMouseReleased(NativeMouseEvent event) {
    }

    private boolean f3KeyPressed = false;
    private boolean sKeyPressed = false;

    @Override
    public void nativeKeyPressed(NativeKeyEvent event) {
        if(!muxVerify.isEnabled()) return;
        if(!muxVerify.getApi().hasGameFocus()) return;
        if(event.getKeyCode() == 61){
            f3KeyPressed = true;
        }
        if(event.getKeyCode() == 31){
            sKeyPressed = true;
        }
        if(sKeyPressed && f3KeyPressed){
            lastLag = System.currentTimeMillis() + 20000;
        }
    }

    @Override
    public void nativeKeyReleased(NativeKeyEvent event) {
        if(!muxVerify.isEnabled()) return;
        if(!muxVerify.getApi().hasGameFocus()) return;
        if(event.getKeyCode() == 61){
            f3KeyPressed = false;
        }
        if(event.getKeyCode() == 31){
            sKeyPressed = false;
        }
    }

    @Override
    public void nativeKeyTyped(NativeKeyEvent event) {


    }
}
