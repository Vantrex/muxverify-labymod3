package me.muxcraft.labymod.verify.anticheat.impl;

import io.netty.buffer.ByteBufUtil;
import me.muxcraft.labymod.verify.anticheat.Detection;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.client.multiplayer.PlayerControllerMP;
import net.minecraft.entity.Entity;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.Vec3;
import net.minecraft.world.WorldSettings;

import java.util.Vector;

public class Reach extends Detection{

    private double lastRange;

    private int i = 0;

    public Reach() {
        super("Reach");
        muxVerify.getApi().getEventManager().registerOnAttack(this::onAttack);
    }

    private int b = 0;

    private void onAttack(Entity entity){

        if(!muxVerify.isEnabled()) return;
        b++;




       if(muxVerify.getMC().playerController.gameIsSurvivalOrAdventure()) {

           final Vec3 vec3 = this.mc.getRenderViewEntity().getPositionEyes(1.0F);
           double hitRange = this.mc.objectMouseOver.hitVec.distanceTo(vec3);

           lastRange = hitRange;

           if (hitRange > 3.0D) {
               onFail("Reach->" + lastRange);
           } else {
               AxisAlignedBB entityBB = entity.getEntityBoundingBox();
               if(entity.getCollisionBorderSize() > 0.1F){
                   onFail("Hitbox");
               }
               double boundingX = (entityBB.maxX + entityBB.minX) / 2;
               // double boundingY = (entityBB.maxY + entityBB.minY) / 2 - 0.89999997615814D; // 0.89999997615814D = real value of the player model y if user has no misplace activated
               double boundingZ = (entityBB.maxZ + entityBB.minZ) / 2;

               if ((boundingX != entity.posX || boundingZ != entity.posZ)) {
                   i++;
                   if (i == 5) {
                       onFail("Misplace->X:" + boundingX + "!" + entity.posX + "Z:" + boundingZ + "!" + entity.posZ);
                       i = 0;
                   }
               } else {
                   if (i > 0) i = 0;
               }
           }
       }
    }
    @Override
    public void onFail(String... s) {
        PacketBuffer packetBuffer = new PacketBuffer(ByteBufUtil.threadLocalDirectBuffer());
        muxVerify.getApi().displayMessageInChat(s[0]);
        packetBuffer.writeString("anticheat_failed=reach>" + s[0]);
        muxVerify.getApi().sendPluginMessage("Mux-Verify",packetBuffer);
    }

}