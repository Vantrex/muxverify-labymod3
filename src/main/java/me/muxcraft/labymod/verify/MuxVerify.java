package me.muxcraft.labymod.verify;

import io.netty.buffer.ByteBufUtil;
import me.muxcraft.labymod.verify.events.labymod.ServerJoinQuitEvent;
import me.muxcraft.labymod.verify.manager.DetectionManager;
import me.muxcraft.labymod.verify.utils.MuxThread;
import me.muxcraft.labymod.verify.utils.A;
import net.labymod.api.LabyModAddon;
import net.labymod.settings.elements.SettingsElement;
import net.minecraft.client.Minecraft;
import net.minecraft.network.PacketBuffer;
import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;


import java.io.IOException;
import java.net.URISyntaxException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.List;

public class MuxVerify extends LabyModAddon{

    private static MuxVerify instance;

    private Minecraft mc;

    private String a;

    private DetectionManager detectionManager;

    private boolean enabled = false;

    private boolean windows;

    private MuxThread muxThread;

    private final List<String> domains = Arrays.asList("muxcraft.eu","muxcraft.de","muxcraft.com","muxcraft.net","muxcraft.minecraft.to","muxcraft","217.198.134.160");

    @Override
    public void onEnable() {

        /* init of the addon */

        System.out.println("Loading LabyMod-Addon \"MuxVerify\"...");


        instance = this;
        windows = userIsOnWindows();
        this.mc = Minecraft.getMinecraft();
        this.muxThread = new MuxThread(this);
        muxThread.start();
        try {
            a = new A().A();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        new ServerJoinQuitEvent(this);
        this.detectionManager = new DetectionManager();
        //new ClientTickEvent(this); // replaced with TimerTask in Detection.java

    }

    @Override
    public void onDisable() {



        if(isWindows()){
        try {
            GlobalScreen.unregisterNativeHook();
        } catch (NativeHookException e) {
            e.printStackTrace();
            }
        }

        if(isEnabled()){
            PacketBuffer packetBuffer = new PacketBuffer(ByteBufUtil.threadLocalDirectBuffer());
            packetBuffer.writeString("addon_disabled");
            getApi().sendPluginMessage("Mux-Verify",packetBuffer);
        }
    }


    @Override
    public void loadConfig() {

    }

    @Override
    protected void fillSettings(List<SettingsElement> list) {

    }

    public static MuxVerify getInstance() {
        return instance;
    }

    public Minecraft getMC() {
        return mc;
    }

    public List<String> getDomains() {
        return domains;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public String getA() {
        return a;
    }

    public DetectionManager getDetectionManager() {
        return detectionManager;
    }

    private boolean userIsOnWindows(){
        return System.getProperty("os.name").toLowerCase().contains("windows");
    }

    public boolean isWindows() {
        return windows;
    }
}
