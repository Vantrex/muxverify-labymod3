package me.muxcraft.labymod.verify.manager;

import me.muxcraft.labymod.verify.anticheat.Detection;
import me.muxcraft.labymod.verify.anticheat.impl.Autoclicker;
import me.muxcraft.labymod.verify.anticheat.impl.ProcessDetection;
import me.muxcraft.labymod.verify.anticheat.impl.Reach;
import me.muxcraft.labymod.verify.anticheat.impl.Velocity;

import java.util.ArrayList;
import java.util.List;

public class DetectionManager {


    private final List<Detection> detectionList;

    private ProcessDetection processDetection;
    private Autoclicker autoclicker;
    private Velocity velocity;

    public DetectionManager(){
        this.detectionList = new ArrayList<>();
        registerDetections();
    }

    private void registerDetections(){
       registerDetection(new Reach());
       registerDetection(processDetection = new ProcessDetection());
       registerDetection(autoclicker = new Autoclicker());
       registerDetection(velocity = new Velocity());
    }

    private void registerDetection(Detection detection){
        this.detectionList.add(detection);
    }

    public void removeDetection(Detection detection){
        this.detectionList.remove(detection);
    }

    public void removeDetection(int index){
        this.detectionList.remove(index);
    }


    public ProcessDetection getProcessDetection() {
        return processDetection;
    }

    public Autoclicker getAutoclicker() {
        return autoclicker;
    }

    public Velocity getVelocity() {
        return velocity;
    }
}
